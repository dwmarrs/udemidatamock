﻿from django.db import models

class Issue(models.Model):
  title = models.CharField(max_length=255)
  description = models.TextField()
  latitude = models.FloatField()
  longitude = models.FloatField()

class Tag(models.Model):
  name = models.CharField(max_length=255)
  issues = models.ManyToManyField(Issue)