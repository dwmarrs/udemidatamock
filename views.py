﻿from issues.models import Issue, Tag
from django.utils import simplejson

def bounded_issues(request):
    
  # Get all of the issues in the area
  """Bounding Latitude and Longitude should have been posted to us
  Latitude increases from Bottom to Top (or South to North)
  Longitude increases from Left to Right (or West to East)"""
  
  """
  If we can filter out the bounded issues
  issues = Issue.objects.filter(latitude__gt=request.POST['latitude_bottom'])
  issues.filter(latitude__lt=request.POST['latitude_top'])
  issues.filter(longitude__gt=request.POST['latitude_left'])
  issues.filter(longitude__lt=request.POST['latitude_right'])
  
  We need something equivalent to...
  SELECT tags.name, COUNT('issues_tags.id') AS num_issues
  FROM tags
  INNER JOIN issues_tags ON tag.id = issues_tags.tag_id
  WHERE issues_tags.issue_id IN (issues.id)
  GROUP BY tags.id
  
  (assuming the lookup table is called issues_tags)
  
  Would this do it?
  """
  
  tags = Tags.objects.filter(issue__latitude__gt=request.POST['latitude_south'],
                             issue__latitude__lt=request.POST['latitude_north'],
                             issue__longitude__gt=request.POST['longitude_west'],
                             issue__longitude__lt=request.POST['longitude_east']
                             ).annotate(num_issues=Count('issue'));
  
  # return a JSON response
  return HttpResponse(simplejson.dumps(tags), mimetype='application/javascript')